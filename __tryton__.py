#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Payment Batch Capture Journal',
    'name_de_DE': 'Fakturierung Rechnungsstellung Stapelerfassung Zahlungen Journal',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Payment Batch Capture Journal
    - Sets the cash journal on invoice lines
''',
    'description_de_DE': ''' Zahlungsjournal für Fakturierung Rechnungsstellung
        Stapelerfassung von Zahlungen
    - Setzt das Zahlungsjournal für Rechnungspositionen
''',
    'depends': [
        'account_invoice_payment_batch_capture',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
