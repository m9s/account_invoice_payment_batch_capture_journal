#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class PaymentLine(ModelSQL, ModelView):
    _name = 'account.invoice.payment_line'

    def on_change_invoice(self, vals):
        journal_obj = Pool().get('account.journal')

        res = super(PaymentLine, self).on_change_invoice(vals)

        args = [('type', '=', 'cash')]
        journal_ids = journal_obj.search(args)
        if len(journal_ids)==1:
            journal = journal_obj.browse(journal_ids[0])
            res['journal'] = journal.id
            res['description'] = journal.name
        return res

PaymentLine()
